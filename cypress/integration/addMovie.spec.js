describe('Add a movie', () => {
  const moviePlot = 'Clarice Starling, an FBI agent, seeks help from Hannibal Lecter, a psychopathic serial killer and former psychiatrist, in order to apprehend another serial killer who has been claiming female victims.';
  const imageUrl = 'https://m.media-amazon.com/images/M/MV5BNjNhZTk0ZmEtNjJhMi00YzFlLWE1MmEtYzM1M2ZmMGMwMTU4XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SY1000_CR0,0,677,1000_AL_.jpg';

  it('should log in to my-movies', () => {
    cy.visit('http://localhost:8080/login')
    cy.get('.card-body #input-1')
      .type('Alin').should('have.value', 'Alin')
    cy.get('.card-body #input-3')
      .type('Test!234')
    cy.get('.card-body button')
      .click()
    cy.url().should('eq', 'http://localhost:8080/')
  });

  it('should navigate to add movie page', () => {
    cy.visit('http://localhost:8080/add-movie')
    cy.url().should('eq', 'http://localhost:8080/add-movie')
    cy.get('.card-body')
      .find('.custom-select')
  });

  it('should enter movie\'s title', () => {
    cy.get('[data-test-id="movie-title"]')
      .type('The Silence of the Lambs').should('have.value', 'The Silence of the Lambs')
  });

  it('should select genres for the movie', () => {
    cy.get('.custom-select')
      .select(['Thriller', 'Horror'])
  });

  it('should enter plot for the movie', () => {
    cy.get('[data-test-id="movie-plot"]')
      .type(moviePlot).should('have.value', moviePlot)
  });

  it('should enter movie stars', () => {
    cy.get('[data-test-id="movie-stars"]')
      .type('Jodie Foster, Anthony Hopkins').should('have.value', 'Jodie Foster, Anthony Hopkins')
  });

  it('should enter director', () => {
    cy.get('[data-test-id="directed-by"]')
      .type('Jonathan Demme').should('have.value', 'Jonathan Demme')
  });

  it('should enter image url', () => {
    cy.get('[data-test-id="image-url"]')
      .type(imageUrl).should('have.value', imageUrl)
  });

  it('should add movie and continue to my-movies', () => {
    cy.get('.btn.btn-block')
      .click()
    cy.url().should('eq', 'http://localhost:8080/my-movies')
  });
});
